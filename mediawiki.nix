{ config, lib, pkgs, ... }:

let
  # Theme
  tweeki = pkgs.stdenv.mkDerivation rec {
    name = "mediawiki-tweeki";
    src = pkgs.fetchFromGitHub {
      owner = "thaider";
      repo = "tweeki";
      rev = "86645842f4abbdc066fabf451021cd0cf3ff5bf2";
      sha256 = "0hvark5jyzhkzv7hhg8dykg07mp2mjqlhfd35v3bh45r8wvsn125";
    };
    installPhase = "mkdir -p $out/Tweeki; cp -R * $out/Tweeki";
  };

  # OAuth (github) login extension.
  #
  # This extension's derivation is... not that clean.
  # This is caused by the fact that it uses composer in quite a tortuous
  # and wrong manner. It includes *one* dependency as a submodule and asks
  # to run `composer install` into that submodule. Yuck.
  #
  # This makes it all work declaratively.
  #
  # Do note that there are two hashes to keep track of:
  #
  #   * The repository's hash
  #   * Our composed hash
  #
  oauth2client = pkgs.stdenv.mkDerivation rec {
    name = "mediawiki-oauth2client";
    _src = pkgs.fetchgit {
      url = "https://github.com/Schine/MW-OAuth2Client.git";
      rev = "c02026a9c0a8ced91f44e62167a5fb515be42d65";
      sha256 = "0h0b9dmjjsvqqs898g3mjxqkly8m8zgynqvvwpn96yq1xspa9f94";
      fetchSubmodules = true;
    };

    composer = pkgs.php71Packages.composer;
    # FIXME : This is not stable... Actually use proper composer integration with nix.
    builder = pkgs.writeScript "${name}--builder" ''
      source $stdenv/setup

      # Copies the source
      cp -R $_src MW-OAuth2Client
      # Ensures a writable copy
      chmod -R +w MW-OAuth2Client

      echo "Composing installation"
      (
          cd MW-OAuth2Client/vendors/oauth2-client
          "${composer}"/bin/composer -n --no-dev --no-autoloader install

          # Affixes times
          find vendor composer.lock -print | while read filename; do
              touch --no-create --date=$SOURCE_DATE_EPOCH "$filename"
          done
      )

      echo "Installing..."
      mkdir -p "$out"/oauth2client
      cp -R MW-OAuth2Client/* "$out"/oauth2client

      stopNest
    '';

    outputHashAlgo = "sha256";
    outputHashMode = "recursive";
    outputHash = "1kwlv1b6d3x2im50qd9fcj6y9aq4j73yc0pdjl9lay6q5jz5ydgc";
  };

  autositemap = pkgs.stdenv.mkDerivation rec {
    name = "mediawiki-AutoSitemap";
    
    src = pkgs.fetchFromGitHub {
      owner = "dolfinus";
      repo = "AutoSitemap";
      rev = "30ed94a47f7fb49ad8442545845a471ea778b066";
      sha256 = "0gama6kq0jbxjgiivy49mlv0k670v55qpprb33fc7kz02rwpis50";
    };
    installPhase = "mkdir -p $out/AutoSitemap; cp -R * $out/AutoSitemap";
  };

  usermerge = pkgs.stdenv.mkDerivation rec {
    name = "mediawiki-UserMerge";
    
    src = pkgs.fetchFromGitHub {
      owner = "wikimedia";
      repo = "mediawiki-extensions-UserMerge";
      rev = "REL1_29";
      sha256 = "0x3y12lfs0rr628bmrnqd9w7b1hbzw9qbmkwnpxhxqva9zc092k8";
    };
    installPhase = "mkdir -p $out/UserMerge; cp -R * $out/UserMerge";
  };

  # Packages our CSS tweaks.
  nix = pkgs.stdenv.mkDerivation rec {
    name = "mediawiki-nix-skin";
    src = ./nix;
    installPhase = "mkdir -p $out/nix; cp -R * $out/nix";
  };

in
{
  services.postgresql = {
    enable = true;
    package = pkgs.postgresql96;
    # https://web.archive.org/web/20160829190636/https://nixos.org/wiki/Setting_up_MediaWiki_%26_vhosts
    # Not magical enough...
    authentication = pkgs.lib.mkOverride 10 ''
        local mediawiki all ident map=mwusers
        local all all ident
    '';
    identMap = ''
        mwusers root mediawiki
        mwusers wwwrun mediawiki
    '';
  };

  # https://github.com/NixOS/nixos-org-configurations/blob/3442c01c56368680aaa9ca3063f2b8144b240a2a/nixos-org/webserver.nix
  services.httpd = {
    enable = true;
    enablePHP = true;
    logPerVirtualHost = true;
    adminAddr="samuel@dionne-riel.localhost";
    extraModules = [
      {
        name = "php7";
        path = "${pkgs.php}/modules/libphp7.so";
      }
    ];

    virtualHosts = [
      {
        hostName = "wiki.${config.networking.hostName}.local";
        extraConfig = ''
          RedirectMatch ^/$ /wiki
        '';
        extraSubservices =
        [
          {
            serviceType = "mediawiki";
            id = "local-nixos";
            siteName = "NixOS Wiki";
            dbType = "postgres";
            extensions = [
              oauth2client
              autositemap
              usermerge
            ];
            skins = [
              tweeki
              # Cheating, and piggy-backing on the skins facility to
              # add files to mediawiki.
              nix
            ];
            defaultSkin = "Tweeki";
            extraConfig = builtins.readFile ./config.php + ''
              $wgPygmentizePath = "${pkgs.python35Packages.pygments}/bin/pygmentize";
            ''
            + "/* Development stuff */ $wgShowExceptionDetails = true;"
            + ''
              ini_set("display_errors", 1); 
              ini_set("display_startup_errors", 1);
              error_reporting(E_ALL);
              ''
            ;
            enableUploads = true;
            # Directory will need to be manually managed, and chowned to wwwrun
            uploadDir = "/srv/local.${config.networking.hostName}.wiki/uploads";
          }
        ];
      }
    ];
  };
}
