#<?php # nix inclusion hack

// FIXME handle skins and extensions loading at module level.

//
// Extensions
// ----------
//

// Special pages
wfLoadExtension("Nuke");
wfLoadExtension("Renameuser");
wfLoadExtension("UserMerge");

// Parser hooks
wfLoadExtension("Cite");
wfLoadExtension("ParserFunctions");
wfLoadExtension("SyntaxHighlight_GeSHi");

// Spam prevention
wfLoadExtensions([ "ConfirmEdit", "ConfirmEdit/ReCaptchaNoCaptcha" ]);

wfLoadExtension("SpamBlacklist");
wfLoadExtension("TitleBlacklist");

// Other
#wfLoadExtension("AutoSitemap");
// No need for MobileFrontend with Tweeki.
wfLoadExtension("oauth2client");
wfLoadExtension("WikiEditor");

//
// ### Extension configuration
//

// Parser functions
// https://www.mediawiki.org/wiki/Extension:ParserFunctions
$wgPFEnableStringFunctions = true;

// ReCaptchaNoCaptcha
$wgCaptchaClass = "ReCaptchaNoCaptcha";
$wgReCaptchaSiteKey = "your public/site key here";
$wgReCaptchaSecretKey = "your private key here";

// See https://github.com/NixOS/nixos-org-configurations/blob/3442c01c56368680aaa9ca3063f2b8144b240a2a/nixos-org/webserver.nix
#  # Prevent pages with blacklisted links.
#  require_once("$IP/extensions/SpamBlacklist/SpamBlacklist.php");
#  $wgSpamBlacklistFiles = array(
#      "http://meta.wikimedia.org/w/index.php?title=Spam_blacklist&action=raw&sb_ver=1"
#  );
#  # Enable DNS blacklisting.
#  $wgEnableDnsBlacklist = true;
#  $wgDnsBlacklistUrls = array("xbl.spamhaus.org");
#  # Require users to answer a question.
#  require_once("$IP/extensions/ConfirmEdit/ConfirmEdit.php");
#  $wgCaptchaTriggers["edit"] = true;
#  $wgCaptchaTriggers["create"] = true;
#  require_once("$IP/extensions/ConfirmEdit/QuestyCaptcha.php");
#  $wgCaptchaClass = "QuestyCaptcha";
#  $arr = array(
#      "What is the name of the Linux distribution to which this wiki is dedicated?" => "NixOS",
#  );
#  foreach ($arr as $key => $value) {
#      $wgCaptchaQuestions[] = array("question" => $key, "answer" => $value);
#  }

$wgOAuth2Client['configuration']['service_name'] = "Github";

// Removes powered byline.
unset($wgFooterIcons["poweredby"]);

//
// Theme
// -----
//

wfLoadSkin("Tweeki");

//
// CSS
// ---
//

// http://tweeki.thai-land.at/wiki/How-Tos#Add_custom_CSS
$wgTweekiSkinCustomCSS[] = "x.nixos-wiki.styles";
$wgResourceModules["x.nixos-wiki.styles"] = array(
	"styles" => array(
		"skins/nix/nixos-site.css" => array("media" => "screen"),
		"skins/nix/tweeki.css" => array("media" => "screen"),
	),
	"localBasePath" => __DIR__,
	"remoteExtPath" => "nixos-wiki",
);

//
// Theme tweaks
// ------------
//

// Hide the edit button as well as the subnavigation (containing the toolbox 
// and personal links for editors) for anonymous users
$wgTweekiSkinHideAnon["EDIT-EXT"] = true;  
$wgTweekiSkinHideAnon["EDIT"] = true;  
$wgTweekiSkinHideAnon["subnav"] = true;
// Always show the combined edit/actions button.
$wgTweekiSkinHideNonAdvanced = ["EDIT-EXT-special" => false];
$wgTweekiSkinHideable[] = "sidebar-right"; 
$wgTweekiSkinHideable[] = "firstHeading"; 
$wgTweekiSkinUseBtnParser = false;
$wgTweekiSkinGridNone = array(
	"mainoffset" => 0,
	"mainwidth" => 12
); 

$wgTweekiSkinSpecialElements = [
	"NIXOS-LOGIN-PROFILE" => "NixOSWiki::login_profile",
	"NIXOS-LOGIN-BETTER" => "NixOSWiki::login_better",
];

class NixOSWiki {
	// This, somehow, either prints the login dropdown
	// or the profile dropdown, depending on the context.
	public static function login_profile($instance, $context, $options) {
		$instance->buildItems("PERSONAL", $options, $context);
		$instance->buildItems("NIXOS-LOGIN-BETTER", $options, $context);
	}

	// This login dropdown tries to be more intelligent in gathering
	// login options. It fails and is hardcoded.
	public static function login_better($instance, $context, $options) {
		$items = $instance->getPersonalTools();

		// If it is needed to be configurable via wiki pages:
		// $items["some-key"]["links"][0]["text"] = wfMessage("nixos-some-key")->plain();
		// Note: I don't know what anonlogin does over login, it's like that in the theme.

		$final_items = [];
		$labels = [
			"login" => "With a wiki account",
			"anonlogin" => "With a wiki account",
		];

		foreach (["login", "anonlogin", "anon_oauth_login"] as $key) {
			if (array_key_exists($key, $items)) {
				if (array_key_exists($key, $labels)) {
					$items[$key]["links"][0]["text"] = $labels[$key];
				}
				$final_items = array_merge($final_items, $items[$key]["links"]);
			}
		}

		if (count($final_items) > 0) {
			$button = array(
				"items" => $final_items,
				"single-id" => "nixos-login-better",
				"text" => "Log in",
			);

			echo TweekiHooks::renderButtons([$button], $options);
		}
	}
}

//
// Local extensions
// ----------------
//
// Those will, for the time being, be shipped through the settings file.
//

$wgHooks["ParserFirstCallInit"][] = "CommandsListingExtension::onParserSetup";

/**
 * Handles <commands> tags.
 * This is *mainly* a convenience to allow themeing of such tags.
 */
class CommandsListingExtension {
	// Register any render callbacks with the parser
	public static function onParserSetup(Parser $parser) {
		// When the parser sees the <commands> tag, it executes renderTagCommands (see below)
		$parser->setHook("commands", "CommandsListingExtension::renderTagCommands");
	}

	// Render <commands>
	// https://phabricator.wikimedia.org/diffusion/ESHG/browse/master/SyntaxHighlight.class.php;08ae879ac6365e6294c97a9116b2d2a1febe00d2$101
	public static function renderTagCommands($text, array $args = [], Parser $parser, PPFrame $frame) {
		// Replace strip markers (For e.g. {{#tag:syntaxhighlight|<nowiki>...}})
		$out = $parser->mStripState->unstripNoWiki($text);
		// Don't trim leading spaces away, just the linefeeds
		$out = preg_replace('/^\n+/', '', rtrim($out));
		$out = htmlspecialchars($out);

		if (isset($args['inline'])) {
			// Enforce inlineness. Stray newlines may result in unexpected list and paragraph processing
			$out = str_replace("\n", ' ', $out);
			$out = Html::rawElement("code", [], $out);
			$out = Html::rawElement("span", ["class" => "commands-listing commands-listing--inline"], $out);
		}
		else {
			$out = str_replace("\n", ' ', $out);
			$out = Html::rawElement("pre", [], $out);
			$out = Html::rawElement("div", ["class" => "commands-listing commands-listing--block"], $out);
		}

		return $out;
	}
}
