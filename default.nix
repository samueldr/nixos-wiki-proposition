{ config, lib, pkgs, ... }:
{
  networking.extraHosts = ''
    10.0.0.10 wiki.${config.networking.hostName}.local
    10.0.0.10 wiki
  '';

  containers."${config.networking.hostName}-mediawiki" = {
    autoStart = false;
    privateNetwork = true;
    hostBridge = "br0";
    localAddress = "10.0.0.10/24";
    config = { config, lib, pkgs, ... }:
    {
      system.stateVersion = "17.03";
      time.timeZone = "America/Montreal";
      networking.firewall.allowedTCPPorts = [ 80 ];
      networking.firewall.allowPing = true;
      networking.defaultGateway = "10.0.0.1";
      imports = [ ./mediawiki.nix ];
    };
  };
}
