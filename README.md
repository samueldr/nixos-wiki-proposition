# Proposal for NixOS wiki configuration

Please see [issue #8](https://github.com/nixos-users/wiki/issues/8).

## Usage

Somewhere that gets included in your configuration.nix

If you want it as a container:

```
{ ... }:

{
  imports = [ 
    ./nixos-wiki-proposition
  ];  
}
```

On the same system (untested)

```
{ ... }:

{
  imports = [ 
    ./nixos-wiki-proposition/mediawiki.nix
  ];  
}
```
